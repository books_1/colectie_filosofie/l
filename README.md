# L

## Content

```
./Lao Zi:
Lao Zi - Cartea despre Dao si putere.pdf
Lao Zi - Cartea despre Tao si virtutile sale.pdf

./Laurentiu Staicu:
Laurentiu Staicu - Metafizica genurilor naturale.pdf

./Laurentiu Stefan Scarlat:
Laurentiu Stefan Scarlat - Dictionar de scrieri politice fundamentale.pdf

./Lawrence Maxwell Krauss:
Lawrence Maxwell Krauss - Universul din nimic.pdf

./Leibniz:
Leibniz - Noi eseuri asupra intelectului omenesc.pdf
Leibniz - Opere filosofice I.pdf

./Leonard Mlodinow & Stephen Hawking:
Leonard Mlodinow & Stephen Hawking - Marele plan.pdf

./Leon Robin:
Leon Robin - Platon.pdf

./Leo Stan & Vlad Puescu:
Leo Stan & Vlad Puescu - Filozofie si dualism (2009).pdf

./Leszek Kolakowski:
Leszek Kolakowski - Horror metaphysicus.pdf
Leszek Kolakowski - Religia.pdf

./Lord Acton:
Lord Acton - Despre libertate.pdf

./Loredana Bocsa:
Loredana Bocsa - Filosofie medievala.pdf

./Louis de Broglie:
Louis de Broglie - Certitudinile si incertitudinile stiintei.pdf

./Lou Marinoff:
Lou Marinoff - Inghite Platon, nu Prozac.pdf
Lou Marinoff - Intrebari fundamentale (Filosofia iti poate schimba viata).pdf

./Luc Ferry:
Luc Ferry - Invata sa traiesti.pdf
Luc Ferry - Noua ordine ecologica.pdf
Luc Ferry - Omul Dumnezeu sau sensul vietii.pdf

./Lucian Blaga:
Lucian Blaga - Despre constiinta filozofica.pdf
Lucian Blaga - Trilogia cosmologica.pdf
Lucian Blaga - Trilogia culturii.pdf
Lucian Blaga - Trilogia cunosterii.pdf
Lucian Blaga - Trilogia valorilor, vol. 1.pdf
Lucian Blaga - Trilogia valorilor, vol. 2.pdf
Lucian Blaga - Trilogia valorilor, vol. 3.pdf

./Lucian Boia:
Lucian Boia - Istorie si mit in constiinta romaneasca.pdf
Lucian Boia - Jocul cu trecutul.pdf
Lucian Boia - Mitul Democratiei.pdf
Lucian Boia - Pentru o istorie a imaginarului.pdf

./Lucius Caecilius Firmianus Lactantius:
Lucius Caecilius Firmianus Lactantius - Despre moartea persecutorilor.pdf

./Ludwig von Mises:
Ludwig von Mises - Capitalismul si dusmanii sai.pdf

./Ludwig Wittgenstein:
Ludwig Wittgenstein - Caietul albastru.pdf
Ludwig Wittgenstein - Cercetari Filozofice.pdf
Ludwig Wittgenstein - Despre certitudine.pdf
Ludwig Wittgenstein - Insemnari postume 1914-1951.pdf
Ludwig Wittgenstein - Jurnale 1914-1916.pdf
Ludwig Wittgenstein - Lectii si convorbiri despre estetica, psihologie si credinta religioasa.pdf
Ludwig Wittgenstein - Tractatus Logico-Philosophicus.pdf
```

